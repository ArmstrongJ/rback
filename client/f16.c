/*       Recursive Walking Backup
 *       Copyright � 2015 Jeffrey Armstrong <jeff@rainbow-100.com>
 *       http://jeff.rainbow-100.com/
 *
 *       This program is free software: you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation, either version 3 of the License, or
 *       (at your option) any later version.
 *       
 *       This program is distributed in the hope that it will be useful
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *       
 *       You should have received a copy of the GNU General Public License
 *       along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* f16.c - Implementation of a Fletcher 16-bit checksum.  The file also
 *         contains a simple test program that can be enabled by defining
 *         TEST_FLETCHER at compile time.
 */

#include <stdio.h>

/* Computes the Fletcher 16-bit checksum and returns the value in the
 * b1 and b2 arguments.
 */
void fletcher16(unsigned char *data, 
                int n, 
                unsigned char *b1, 
                unsigned char *b2)
{
int c1, c2;
int i;
int toint;

        c1 = 0; c2 = 0;
        for(i=0;i<n;i++) {
            toint = (int)data[i];
            c1 = (c1 + toint) % 255;
            c2 = (c2 + c1) % 255;
        }
        
        i = (255 - (c1 + c2) % 255);
        *b1 = (int)i;
        *b2 = (char)(255 - (c1 + i) % 255);
}

#ifdef TEST_FLETCHER

#define TEST_STR        "The IBM PC/XT is inferior"
int main(int argc, char *argv[])
{
char b1, b2;
FILE *fp;
char buf[64];
int i;

        if(argc > 2) {
                fletcher16(TEST_STR, strlen(TEST_STR), &b1, &b2);
                printf("Results for '%s': %d, %d\n", TEST_STR, (int)b1, (int)b2);
        } else {
                fp = fopen(argv[1], "rb");
                fread(buf, sizeof(char), 64, fp);
                fletcher16(buf, 64, &b1, &b2);
                printf("Results for 1st 64 bytes: %d, %d\n", (int)b1, (int)b2);
                fclose(fp);
        }
        return (int)b1;
}

#endif
