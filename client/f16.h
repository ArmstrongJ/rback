#ifndef _F16_HDR_
#define _F16_HDR_

void fletcher16(unsigned char *data, 
                int n, 
                unsigned char *b1, 
                unsigned char *b2);

#endif /* _F16_HDR_ */
