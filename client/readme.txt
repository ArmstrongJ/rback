Backup Tree Walker
==================

This software will recursively walk a directory tree and transmit every
file encountered to server software via a serial port.  The transmission
protocol is quite basic.  When a file is encountered, the name of the
file, in absolute path format, is transmitted to the server followed by
a semicolon.  When the server responds with "OK" followed by a semicolon,
this program sends a single byte of how many bytes it will send, 64 by
default.  After the block length, it sends the block.  Finally, it sends
two bytes representing a Fletcher 16 checksum.  This program then waits
until the server responds with "OK," "RETRY," or "ABORT" followed by a
semicolon.  On "OK," the server repeats the process with another block.

Requirements
------------

The client software requires FosLib, available at:

    https://bitbucket.org/ArmstrongJ/foslib
    
Make sure, when building the library above, to use the same compiler
and memory model.  This code works perfectly with the small memory
model.

Building
--------

The code has only recently been tested with Borland Turbo C 2.0.  To
compile with Turbo C, use the following:

    tcc -ms -DTURBOC walker.c f16.c fossil.lib

The command above compiles using a small memory model.  The "-DTURBOC"
flag is necessary so that the proper directory search routines are used.

If compiling with Microsoft C or, possibly, Open Watcom, a similar 
command can be used without the "-DTURBOC" flag.

Executing
---------

The backup program, WALKER.EXE, accepts two mandatory arguments, the FOSSIL
driver port and the speed in bits per second.  A third, optional argument
specifies the directory in which to start the recursive traversal.  By
default, the routine will always start in E: since most people are probably
using this software on a Rainbow 100.  If you're using a more obscure system
like an IBM PC, you'll want to specify the third argument.

To transfer everything on drive E: over COM port 1 (FOSSIL port 0) at 9600bps,
use the following command:

        walker 0 9600
        
To transfer everything in the F:\SRC directory at speed 19200, use the
following command:

        walker 0 19200 F:\SRC
        
Note that the speeds are limited by what FOSSIL drivers support.  The top
speed is 38400.

