Serial Backup Server
====================

This Python software acts as a server for receiving files via the 
included client program.

Requirements
------------

The server software requires pyserial to be installed:

    https://github.com/pyserial/pyserial
    
The server software itself should run fine on both Python 2.7 and Python 
3.3 or higher.

Executing
---------

The server software can be started simply with the command:

    python rback.py
    
The command above will start the server listening on port /dev/ttyUSB0 
with a speed of 19200 bps.  Alternatively, port and speed can be 
specified:

    python rback.py -p /dev/ttyACM0 -s 9600
    
The above command is obvious.

The server should be started before the client since it waits for the 
'ONLINE' indicator from the client before receiving files.

The program will store transfered files in the client systems directory 
tree within the current directory.  For example, if the client sends:

    E:\MSDOS\EDLIN.EXE
    
the server will store the file in:

    <current directory>/E/MSDOS/EDLIN.EXE
    
The server software will overwrite anything already present.
