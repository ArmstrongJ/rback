Serial Backup Client/Server
===========================

This software is designed to backup MS-DOS systems over a 
serial link to a modern-ish computer.  The software is 
comprised of two components: a C client program running 
on MS-DOS and a Python server script that listens for and 
accepts files over a serial link.  The software is 
designed for minimal user interaction so that a MS-DOS 
machine may be backed up in its entirety, including 
preserving the directory structure.

The program uses a simplistic protocol that should be 
mildly, but not particularly, robust.  Everything should 
work smoothly on a null modem link, but it might 
encounter trouble over a phone line.  The protocol, which 
sends a block followed by a checksum, is not resilient 
against dropped bytes, for example.

Because the tranfers are done via a serial link, one 
should expect the full backup to take a substantial 
amount of time to complete.  20MB at 19200bps will take 
several hours to complete.  

This software was written for a personal need.  However, 
I'll be happy to provide any improvements if anyone has a 
suggestion or two.

---
Jeffrey Armstrong
October 9, 2015

License
=======

Serial Backup Server
Copyright (C) 2015 Jeffrey Armstrong <jeff@rainbow-100.com>
http://jeff.rainbow-100.com/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
       
This program is distributed in the hope that it will be useful
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
      
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
